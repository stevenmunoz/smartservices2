(function () {
    'use strict';

    angular.module('iot').controller('FiltersCtrl', ['$scope', '$rootScope', '$state', 'DSCacheFactory', 'homeApi', 'config', FiltersCtrl]);

    function FiltersCtrl ($scope, $rootScope, $state, DSCacheFactory, homeApi, config) {

    	var vm = this;

        /*vm.getImages = function (forceRefresh) {

            homeApi.getSlider(forceRefresh).then(function (data) {
                vm.slider = data;   
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }

        vm.getImages(false);*/

        vm.clickDivEstiloVida1 = function(){
            if(vm.itemStyle1 == "divEstilosdVidaBorder"){
                vm.itemStyle1="divEstilosVidaGlobal";
            }
            else{
                vm.itemStyle1="divEstilosdVidaBorder";
            }
            
            
        }

        vm.clickDivEstiloVida2 = function(){
            if(vm.itemStyle2 == "divEstilosdVidaBorder"){
                vm.itemStyle2="divEstilosVidaGlobal";
            }
            else{
                vm.itemStyle2="divEstilosdVidaBorder";
            }
        }

        vm.clickDivEstiloVida3 = function(){
            if(vm.itemStyle3 == "divEstilosdVidaBorder"){
                vm.itemStyle3="divEstilosVidaGlobal";
            }
            else{
                vm.itemStyle3="divEstilosdVidaBorder";
            }
        }

        vm.clickDivEstiloVida4 = function(){
            if(vm.itemStyle4 == "divEstilosdVidaBorder"){
                vm.itemStyle4="divEstilosVidaGlobal";
            }
            else{
                vm.itemStyle4="divEstilosdVidaBorder";
            }
        }

        vm.clickDivEstiloVida5 = function(){
            if(vm.itemStyle5 == "divEstilosdVidaBorder"){
                vm.itemStyle5="divEstilosVidaGlobal";
            }
            else{
                vm.itemStyle5="divEstilosdVidaBorder";
            }
        }

        vm.clickDivEstiloVida6 = function(){
            if(vm.itemStyle6 == "divEstilosdVidaBorder"){
                vm.itemStyle6="divEstilosVidaGlobal";
            }
            else{
                vm.itemStyle6="divEstilosdVidaBorder";
            }
        }

        vm.ResultadosBusqueda = function(){
            $state.go("router.resultados"); 
        }
    }
})();