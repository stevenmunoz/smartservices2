(function () {
    'use strict';

    angular.module('iot').controller('DriverCtrl', ['$scope', '$stateParams', 'driverApi', 'config', 'DSCacheFactory', DriverCtrl]);

    function DriverCtrl($scope, $stateParams, driverApi, config, DSCacheFactory) {
        
        var vm = this;

        vm.dataRate = {
            plate: '',
            type: '',
            company: '',
            observation: ''
        }

        $scope.items = [
            { id: 1, name: 'Bus' },
            { id: 2, name: 'Buseta' },
            { id: 3, name: 'Taxi' },
            { id: 4, name: 'Particular' },
            { id: 5, name: 'Moto' },
            { id: 6, name: 'Bicicleta' }
        ];

        vm.loadStar = function(num) {
            console.log("ok");
            var number;
            for (var i = 1; i <= 5; i++) {
                if (i <= num) {
                    number = 'img/estrella_llena.png'
                }
                else {
                    number = 'img/estrella_vacia.png'
                }
                $('#star' + i).attr('src', number);
            }
        };

        vm.sendDriveInfo = function (){
            
            driverApi.sendRate(vm.dataRate).then(function (data) {
                vm.dataRate.plate = "";
                vm.dataRate.type = "";
                vm.dataRate.company = "";
                vm.dataRate.observation = "";
            });   
        };

    };
})();