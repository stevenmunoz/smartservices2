(function () {
    'use strict';

    angular.module('iot').controller('pointsCtrl', ['$scope', '$state', '$ionicLoading', '$timeout', 'pointsApi', 'countriesApi','config', '$cordovaGeolocation', '$ionicPopup', '$window', pointsCtrl]);

    /*
        Description:
        Creates a new points controller function
    */
    function pointsCtrl($scope, $state, $ionicLoading, $timeout, pointsApi, countriesApi, config, $cordovaGeolocation, $ionicPopup, $window) {
       
       var vm = this;
       vm.wazeCoordinates;
       vm.clickCoordinates;
       vm.positionCoordinates;
       vm.mapClass = 'tag-hidden';

       function initializeMap (centerLatitude, centerLongitude) {

         //map configuration
         vm.map = {
              center: {
                  latitude: centerLatitude,
                  longitude: centerLongitude
              },
              zoom: config.zoom,
              markers: [],
              options: {
                  scrollwheel: false,
              },
              eventsProperty : {
                  /*click : function(mapModel, eventName, originalEventArgs) {
                      //console.log("user defined event on map directive with scope", this);
                      //console.log("user defined event: " + eventName, mapModel, originalEventArgs);

                      var e = originalEventArgs[0];
                      var lat = e.latLng.lat(),
                      lon = e.latLng.lng();

                      var clickCoord = {
                        latitude: lat,
                        longitude: lonre
                      };

                      vm.clickCoordinates = clickCoord;

                      console.log('You clicked here ' + 'lat: ' + lat + ' lon: ' + lon);
                      vm.showRegister();
                  }*/
              }
          };
      }

      var posOptions = {timeout: 10000, enableHighAccuracy: false};
        
      function getCurrenPosition () {

          $cordovaGeolocation
          .getCurrentPosition(posOptions)
          .then(function (position) {
              var lat  = position.coords.latitude;
              var long = position.coords.longitude;

              var positionCoord = {
                latitude: lat,
                longitude: long
              };

              vm.positionCoordinates = positionCoord;

              console.log("Current position" + lat + "and long " + long);
              initializeMap(lat, long);
              //loadPoints();

          }, function(err) {
              console.log(err);
              //loadPoints();
          });
        }  
       
        /*
            Description:
            Get data from service and create markers in map
        */
        function loadPoints(){
            
            console.log("load points");
            pointsApi.getPoints().then(function (data){
               vm.points = data;
               _.each(data, function(item) {
                    //console.log(item);
                    var infoWindow = '<div>' + item.description + '<br>';
                   $timeout(function() {
                        vm.map.markers.push(createMarker(item.latitude, item.longitude, item.id, item.name, infoWindow));
                    }, 1000);
                });

            });  
        };

        function getReportType(){
            
            console.log("Report Type");
            pointsApi.getReportType().then(function (data){
               vm.reportType = data;
            }); 
        };

        vm.imgClick = function (){
          loadPoints();
          vm.findClass = 'tag-hidden';
          vm.mapClass = 'tag-visible';
        }

        /*
            Description:
            Handle location click and shows route using mobile selected geo service eg. waze, google maps
        
            Params:
            @marker: marker with data
        */
       
        vm.handleLocation = function(){
            $window.open("waze://?ll="+vm.wazeCoordinates.latitude+","+vm.wazeCoordinates.longitude+"&navigate=yes");
        };

       /*
            Description:
            Load all states and assign to vm object to populate select list
        */ 
       vm.loadStates = function (){
           countriesApi.getStates().then(function (data){
               vm.states = data.states;
           });
       };

       /*
           Description:
           Load all cities and assign it to vm object to populate select list
       */
       vm.getCities = function (){
            vm.cities = {};
            countriesApi.getCities(vm.country.region_id).then(function (data){
               vm.cities = data.cities;
           });
       };

       vm.getNearestPoint = function (){
           getLocationByName(vm.city.city_name);
       };

       vm.showRegister = function() {
            $scope.data = {}
            var myPopup = $ionicPopup.show({
                template: '<select style="width:100%;" ng-model="data.reportType" ng-options="(report.text) for report in vm.reportType track by report.id"required></select> <br > <span style="float:right;color:red;">{{100 - data.comment.length}} Caracteres</span><textarea style="margin-top: 12px;" rows="5" name="comment" ng-model="data.comment" placeholder="Describe brevemente el incidente" ng-trim="false" maxlength="100"></textarea>',
                title: 'Reportar Incidente',
                subTitle: 'Ingresa los datos para completar el registro',
                scope: $scope,
                buttons: [
                  { text: 'Cancelar' },
                    {
                      text: '<b>Enviar</b>',
                      type: 'buttonDiagnostic',
                      onTap: function(e) {
                          if (!$scope.data.reportType && !$scope.data.comment) {
                             e.preventDefault();
                          } else {
                             //return $scope.data.email;
                             $scope.data.coordinates = vm.clickCoordinates;
                             vm.dataPoint = $scope.data;
                             vm.sendPoint();
                          }
                      }
                    },
                ]
           });
        };

         vm.showMarkerPopup = function(reportType, Description, latitude, longitude) {
            $scope.data = {};

        var wazeCoor = {
                latitude: latitude,
                longitude: longitude
              };

              vm.wazeCoordinates = wazeCoor;

            var template = '<div class="list card">'+

      '<div class="item item-image">'
        +'<img src="img/Foto1.jpg" style="height:100px;">'
      +'</div>'
      +'<br />'
      +'<label class="lblForm" style="vertical-align: super; font-weight:200; font-size:14px;">'
        +'&nbsp;&nbsp;Cra. 54 No. 33 sur - 20'
        +'<br />'
        +'&nbsp;&nbsp;La frontera - Envigado'
        +'<br />'
        +'&nbsp;&nbsp;Arrendamiento'
      +'</label>'
      +'<br />'
      +'<a class="item item-icon-left assertive" style="float:left; width:100%; padding-left:6%;">'
        +'<img src="img/Mascotas-icon-negro.png" style="margin-right:5%;">'
        +'<label class="lblForm" style="vertical-align: super; margin-right:5%; font-size:12px;">'
        +'Mascotas'
      +'</label>'
      
        +'<img src="img/Rumba-icon-negro.png" style="margin-right:5%;">'
        +'<label class="lblForm" style="vertical-align: super; font-size:12px;">'
        +'Rumba'
      +'</label>'

      +'</a>'
      +'<a class="item item-icon-left assertive" style="float:left; width:50%; padding-left:6%; background-color:#38cff2; text-align:center; height:60px;" ng-click="vm.handleLocation()">'
        +'<img src="img/wase-icon.png" style="margin-right:5%;">'
        
      +'</a>'
      +'<a class="item item-icon-left assertive" style="float:left; width:50%; padding-left:6%; background-color:#38cff2; text-align:center; height:60px;">'
        +'<img src="img/eye-icon.png" style="margin-right:5%;">'
       
      
      +'</a>'

    +'</div>';

            //Calculate distance

            var distance = getDistanceFromLatLonInKm(vm.positionCoordinates.latitude, vm.positionCoordinates.longitude, latitude, longitude);
            console.log(distance);

            var myPopup = $ionicPopup.show({
                template: template,
                title: 'a '+Math.round(distance)+' kms de distancia',
                subTitle: '',
                scope: $scope,
                buttons: [
                  { text: 'Regresar',
                    type: 'buttonDiagnostic'
                  }
                ]
           });
        };

        vm.sendPoint = function (){
            
            pointsApi.sendPoint(vm.dataPoint).then(function (data) {
                vm.dataPoint.reportType = "";
                vm.dataPoint.comment = "";
                loadPoints();

                //Redireccionamos a la página de historias
                $state.go("router.locations");
            });   
        };

         /*
           Description:
           Create a marker given certain parameteres
            
           Params: 
           @latitude: marker latitude
           @longitude: marker longitude
           @markerId: marker id
           @labelContent: marker label
           @infoWindowContent: infowindow string
       */
      
        function createMarker(latitude, longitude, markerId, labelContent, infoWindowContent, icon) {

            var latitude = latitude,
                longitude = longitude;

            if (markerId == null) {
                markerId = "id";
            }

            var ret = {
                latitude: latitude,
                longitude: longitude,
                animation: 1,
                labelAnchor: "28 -5",
                labelClass: 'marker-labels',
                labelContent: labelContent,
                content: infoWindowContent,
                show: false,
                id: markerId,
                icon: 'img/Pin-maps.png'
            };

            ret.onClick = function() {
                console.log("Clicked Marker!");
                vm.showMarkerPopup(1, infoWindowContent, latitude, longitude);
                //ret.show = !ret.show;
            };

            ret[markerId] = markerId;

            return ret;        
        }

        /*
            Description:
            Get coordinates from one location by name
        
            Params:
            @cityName: place to find
        */
       function getLocationByName(cityName){
           
           var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': cityName}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    paintNearestPoint(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                }
            });
       }

       /*
           Description:
           Reload map and paint new point 

           Params:
           @latitude: marker latitude
           @longitude: marker longitude
           
       */
       function paintNearestPoint (latitude, longitude) {

            pointsApi.getNearestPoint(latitude, longitude).then(function (data){
               var nearestPoint = data.GrupoTerritorial;
               var infoWindow = '<div>' + nearestPoint.Direccion + '<br>' + nearestPoint.TelefonoPrincipal +'</div>'
               
               if(nearestPoint.Latitud == 0 && nearestPoint.Longitud == 0) {
                    $ionicLoading.show({
                        template: 'No se han encontrado un punto cercano. Intente con otra ubicación.'
                    });

                    $timeout(function() {
                        $ionicLoading.hide();
                    }, 2000);

               } else {
                    var center = {
                        latitude: 0,
                        longitude: 0
                    };

                    center.latitude = nearestPoint.Latitud;
                    center.longitude = nearestPoint.Longitud;
                    vm.map.markers = [];  
                    vm.map.center = center;  
                    vm.map.markers.push(createMarker(nearestPoint.Latitud, nearestPoint.Longitud, nearestPoint.ID, nearestPoint.Nombre, infoWindow)); 
               }
               
           });

       };

      $scope.rotate = function (angle) {
          $scope.angle = angle;
      };

      function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
          var R = 6371; // Radius of the earth in km
          var dLat = deg2rad(lat2-lat1);  // deg2rad below
          var dLon = deg2rad(lon2-lon1); 
          var a = 
              Math.sin(dLat/2) * Math.sin(dLat/2) +
              Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
              Math.sin(dLon/2) * Math.sin(dLon/2)
              ; 
          var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
          var d = R * c; // Distance in km
          return d;
      }

      function deg2rad(deg) {
        return deg * (Math.PI/180)
      }

      getReportType();
      initializeMap(config.latitude, config.longitude);
      vm.loadStates();
      getCurrenPosition();

      $scope.rotate('45')
        
    };
})();