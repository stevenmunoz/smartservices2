(function(){
	'use strict';

	angular.module('iot').factory('newsApi', ['$http', '$q', 'DSCacheFactory', '$ionicLoading', 'config', newsApi]);

	function newsApi ($http, $q, DSCacheFactory, $ionicLoading, config) {

		self.newsDataCache = DSCacheFactory.get("newsDataCache");
		
		self.newsDataCache.setOptions({
            onExpire: function (key, value) {
                getNews(true)
                    .then(function () {
                        console.log("News data Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting news data. Putting expired item back in the cache.", new Date());
                        self.newsDataCache.put(key, value);
                    });
            }
        });

         function getNews (forceRefresh) {

        	$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

        	var deferred = $q.defer(),
        		requestUrl = config.backendEndpoint,
				cacheKey = "news",
				newsData = null;

			var req = {
				method: 'GET',
				url: requestUrl + config.getNews
			};
			
			//Request for news

			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

		     	if (!forceRefresh) {
					newsData = self.newsDataCache.get(cacheKey);
				}

				if (newsData) {
					$ionicLoading.hide();
					console.log("Found news data inside cache");
					deferred.resolve(newsData);

				} else {
						
					$ionicLoading.show({
		              	template: 'Cargando Información ...'
		            });

					$http(req).success(function (data) {
						$ionicLoading.hide();
						self.newsDataCache.put(cacheKey, data.result.news);
						deferred.resolve(data.result.news);

					}).error(function (data) {
							
						$ionicLoading.hide();
						newsData = self.newsDataCache.get(cacheKey);

						if(newsData) {
							deferred.resolve(newsData);
						} else {
							deferred.reject();	
						}
					});
				}

				return deferred.promise;
	    }

        return {
        	getNews: getNews,
        };
	}

})();