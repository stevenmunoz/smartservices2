(function(){
	'use strict';

	angular.module('iot').factory('faqsApi', ['$http', '$q', 'DSCacheFactory', '$ionicLoading', 'config', faqsApi]);

	function faqsApi ($http, $q, DSCacheFactory, $ionicLoading, config) {

		self.faqsDataCache = DSCacheFactory.get("faqsDataCache");

		self.faqsDataCache.setOptions({
            onExpire: function (key, value) {
                getFaqs(true)
                    .then(function () {
                        console.log("Faqs Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting data. Putting expired item back in the cache.", new Date());
                        self.faqsDataCache.put(key, value);
                    });
            }
        });

        function getFaqs (forceRefresh) {

        	$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

        	var deferred = $q.defer(),
        		requestUrl = config.backendEndpoint,
				cacheKey = "faqs",
				faqsData = null;

			var req = {
				method: 'GET',
				url: requestUrl + config.getFaqs
			};
			
			//Request for faqs
			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

		     	if (!forceRefresh) {
					faqsData = self.faqsDataCache.get(cacheKey);
				}

				if (faqsData) {
					$ionicLoading.hide();
					console.log("Found faqs data inside cache");
					deferred.resolve(faqsData);

				} else {
						
					$ionicLoading.show({
		              	template: 'Cargando Información ...'
		            });

					$http(req).success(function (data) {
						$ionicLoading.hide();
						self.faqsDataCache.put(cacheKey, data.result.preguntasFrecuentes);
						deferred.resolve(data.result.preguntasFrecuentes);

					}).error(function (data) {
							
						$ionicLoading.hide();
						faqsData = self.faqsDataCache.get(cacheKey);

						if(faqsData) {
							deferred.resolve(faqsData);
						} else {
							deferred.reject();	
						}
					});
				}

				return deferred.promise;
	    }	

        return {
        	getFaqs: getFaqs
        };
	}

})();