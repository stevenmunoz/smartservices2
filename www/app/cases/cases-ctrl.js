(function () {
    'use strict';

    angular.module('iot').controller('casesCtrl', ['$scope', '$rootScope', '$state', 'casesApi', '$cordovaInAppBrowser', '$cordovaCamera', '$ionicPopup', 'config', casesCtrl]);

    function casesCtrl ($scope, $rootScope, $state, casesApi, $cordovaInAppBrowser, $cordovaCamera, $ionicPopup, config) {

        var vm = this;

        var options = config.inAppOptions;

        vm.loadCases = function (forceRefresh) {

            casesApi.getCases(forceRefresh).then(function (data) {
                vm.cases = data;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }

        vm.browse = function(url) {
    		$cordovaInAppBrowser.open(url, '_system', options)
    		      .then(function(event) {
    		        // success
    		      })
    		      .catch(function(event) {
    		        // error
    		      });
    		};

        vm.showRegister = function() {
            $scope.data = {}
            var myPopup = $ionicPopup.show({
                template: '<label>Título</label><span style="float:right;color:red;">{{50 - data.title.length}} Caracteres</span><input type="text" name="title" placeholder="Título de la historia" ng-model="data.title" ng-trim="false" maxlength="50" /> <br> <label>Categoría</label><select style="width:100%;" ng-model="data.category" ng-options="(category.text) for category in vm.categoryDiagnostic track by category.id"required></select> <br><br> <label>Descripción</label><span style="float:right;color:red;">{{100 - data.description.length}} Caracteres</span><textarea rows="5" name="description" placeholder="Describe brevemente tu historia" ng-model="data.description" ng-trim="false" maxlength="100"></textarea>',
                title: 'Registra tu Historia',
                subTitle: 'Ingresa los datos para completar el registro',
                scope: $scope,
                buttons: [
                    { text: 'Cancelar' },
                    {
                      text: '<b>Registrar</b>',
                      type: 'buttonDiagnostic',
                      onTap: function(e) {
                          if (!$scope.data.title && !$scope.data.description) {
                             e.preventDefault();
                          } else {

                             //Seteo la imagen pormdefecto para hacer la prueba
                             $scope.data.image = "http://sd.keepcalm-o-matic.co.uk/i/keep-calm-and-drive-safe-41.png";

                             vm.dataCase = $scope.data;
                             vm.sendCase();
                          }
                      }
                    },
                ]
           });
        };

        function getCategoryDiagnostic(){
            
            console.log("Category Diagnostic");
            casesApi.getCategoryDiagnostic().then(function (data){
               vm.categoryDiagnostic = data;
            }); 
        };

        vm.sendCase = function (){
            
            casesApi.sendCase(vm.dataCase).then(function (data) {
                vm.dataCase.title = "";
                vm.dataCase.description = "";
                vm.dataCase.image = "";

                //Redireccionamos a la página de historias
                vm.loadCases(true);
            });   
        };

        vm.loadCases(false);
        getCategoryDiagnostic();
    }
})();