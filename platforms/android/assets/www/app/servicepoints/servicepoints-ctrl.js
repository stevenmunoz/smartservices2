(function () {
    'use strict';

    angular.module('iot').controller('pointsCtrl', ['$scope', '$ionicLoading', '$timeout', 'pointsApi', 'countriesApi','config', '$cordovaGeolocation', '$ionicPopup', pointsCtrl]);

    /*
        Description:
        Creates a new points controller function
    */
    function pointsCtrl($scope, $ionicLoading, $timeout, pointsApi, countriesApi, config, $cordovaGeolocation, $ionicPopup) {
       
       var vm = this;
       
       function initializeMap (centerLatitude, centerLongitude) {

         //map configuration
         vm.map = {
              center: {
                  latitude: centerLatitude,
                  longitude: centerLongitude
              },
              zoom: config.zoom,
              markers: [],
              options: {
                  scrollwheel: false
              },
              eventsProperty : {
                  click : function(mapModel, eventName, originalEventArgs) {
                      //console.log("user defined event on map directive with scope", this);
                      //console.log("user defined event: " + eventName, mapModel, originalEventArgs);
                      var e = originalEventArgs[0];
                      var lat = e.latLng.lat(),
                      lon = e.latLng.lng();
                      console.log('You clicked here ' + 'lat: ' + lat + ' lon: ' + lon);
                      vm.showRegister();
                  }
              }
          };
      }
      
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
        
      function getCurrenPosition () {

          $cordovaGeolocation
          .getCurrentPosition(posOptions)
          .then(function (position) {
              var lat  = position.coords.latitude
              var long = position.coords.longitude
              console.log("Current position" + lat + "and long " + long);
              initializeMap(lat, long);
              loadPoints();
          }, function(err) {
              console.log(err);
              loadPoints();
          });
        }  
       
        /*
            Description:
            Get data from service and create markers in map
        */
        function loadPoints(){
            
            console.log("load points");
            pointsApi.getPoints().then(function (data){
               vm.points = data;
               _.each(data, function(item) {
                    //console.log(item);
                    var infoWindow = '<div>' + item.description + '<br>'
                    vm.map.markers.push(createMarker(item.latitude, item.longitude, item.id, item.name, infoWindow));
                });

            });  
        };

        function getReportType(){
            
            console.log("Report Type");
            pointsApi.getReportType().then(function (data){
               vm.reportType = data;
            }); 
        };

        /*
            Description:
            Handle location click and shows route using mobile selected geo service eg. waze, google maps
        
            Params:
            @marker: marker with data
        */
        vm.handleLocation = function (marker){
             window.location = "geo:" + marker.latitude + "," + marker.longitude + ";u=35";
        }

       /*
            Description:
            Load all states and assign to vm object to populate select list
        */ 
       vm.loadStates = function (){
           countriesApi.getStates().then(function (data){
               vm.states = data.states;
           });
       };

       /*
           Description:
           Load all cities and assign it to vm object to populate select list
       */
       vm.getCities = function (){
            vm.cities = {};
            countriesApi.getCities(vm.country.region_id).then(function (data){
               vm.cities = data.cities;
           });
       };

       vm.getNearestPoint = function (){
           getLocationByName(vm.city.city_name);
       };

       vm.showRegister = function() {
            $scope.data = {}
            var myPopup = $ionicPopup.show({
                template: '<select ng-model="data.receptor" ng-options="(report.text) for report in vm.reportType track by report.id"required></select> <br > <textarea style="margin-top: 12px;" name="comment" ng-model="data.comment" placeholder="Describe brevemente el incidente"></textarea>',
                title: 'Reportar Incidente',
                subTitle: 'Ingresa los datos para completar el registro',
                scope: $scope,
                buttons: [
                  { text: 'Cancelar' },
                    {
                      text: '<b>Enviar</b>',
                      type: 'buttonDiagnostic',
                      onTap: function(e) {
                          if (!$scope.data.comment) {
                             e.preventDefault();
                          } else {
                             return $scope.data.email;
                          }
                      }
                    },
                ]
           });
        };

       /*
           Description:
           Create a marker given certain parameteres
            
           Params: 
           @latitude: marker latitude
           @longitude: marker longitude
           @markerId: marker id
           @labelContent: marker label
           @infoWindowContent: infowindow string
       */
       function createMarker(latitude, longitude, markerId, labelContent, infoWindowContent) {
            var marker = {
                
                options: {
                    animation: 0,
                    labelAnchor: "28 -5",
                    labelClass: 'marker-labels',
                    labelContent: labelContent,
                    content: infoWindowContent
                },
                latitude: latitude,
                longitude: longitude,
                id: markerId          
            };

            return marker;        
        }

        /*
            Description:
            Get coordinates from one location by name
        
            Params:
            @cityName: place to find
        */
       function getLocationByName(cityName){
           
           var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': cityName}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    paintNearestPoint(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                }
            });
       }

       /*
           Description:
           Reload map and paint new point 

           Params:
           @latitude: marker latitude
           @longitude: marker longitude
           
       */
       function paintNearestPoint (latitude, longitude) {

            pointsApi.getNearestPoint(latitude, longitude).then(function (data){
               var nearestPoint = data.GrupoTerritorial;
               var infoWindow = '<div>' + nearestPoint.Direccion + '<br>' + nearestPoint.TelefonoPrincipal +'</div>'
               
               if(nearestPoint.Latitud == 0 && nearestPoint.Longitud == 0) {
                    $ionicLoading.show({
                        template: 'No se han encontrado un punto cercano. Intente con otra ubicación.'
                    });

                    $timeout(function() {
                        $ionicLoading.hide();
                    }, 2000);

               } else {
                    var center = {
                        latitude: 0,
                        longitude: 0
                    };

                    center.latitude = nearestPoint.Latitud;
                    center.longitude = nearestPoint.Longitud;
                    vm.map.markers = [];  
                    vm.map.center = center;  
                    vm.map.markers.push(createMarker(nearestPoint.Latitud, nearestPoint.Longitud, nearestPoint.ID, nearestPoint.Nombre, infoWindow)); 
               }
               
           });

       };

        getReportType();
        initializeMap(config.latitude, config.longitude);
        vm.loadStates();
        getCurrenPosition();
        
    };
})();