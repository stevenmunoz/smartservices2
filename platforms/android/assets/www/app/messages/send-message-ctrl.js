(function () {
    'use strict';

    angular.module('iot').controller('sendMessageCtrl', ['$scope', '$stateParams', 'messagesApi', 'pprsApi', 'config', 'DSCacheFactory', sendMessageCtrl]);

    function sendMessageCtrl($scope, $stateParams, messagesApi, pprsApi, config, DSCacheFactory) {
        
        var vm = this;
        self.userDataCache = DSCacheFactory.get("userDataCache");

        vm.role = self.userDataCache.get("roleId");

        vm.dataMessage = {
            receptor: '',
            title: '',
            content: ''
        }

        function loadPprsSelect(forceRefresh, idProfessional) {

            pprsApi.getPprs(forceRefresh, idProfessional).then(function (data) {

                vm.users = data;
            });    
        }


        function getProfessionalByPpr (idPpr){
            
            pprsApi.getProfessionalByPpr(idPpr).then(function (data) {

                var array = [];
                array.push(data);
            
                vm.professionals = array;
            });   
        }

        vm.sendMessage = function (){
            
            messagesApi.sendMessage(vm.dataMessage).then(function (data) {
                vm.dataMessage.title = "";
                vm.dataMessage.content = "";
                vm.dataMessage.receptor = "";
            });   
        };
        
        if (vm.role == 0) {
            
            loadPprsSelect(true, self.userDataCache.get("id"));    
        } 
        else if (vm.role == 1) {

            getProfessionalByPpr(self.userDataCache.get("id"));
        }
        
        //vm.loadDetail();
    };
})();