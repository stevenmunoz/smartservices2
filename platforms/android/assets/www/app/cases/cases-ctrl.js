(function () {
    'use strict';

    angular.module('iot').controller('casesCtrl', ['$scope', '$rootScope', '$state', 'casesApi', 'newsApi', '$cordovaInAppBrowser', 'config', casesCtrl]);

    function casesCtrl ($scope, $rootScope, $state, casesApi, newsApi, $cordovaInAppBrowser, config) {

        var vm = this;

        var options = config.inAppOptions;

        vm.loadCases = function (forceRefresh) {

            casesApi.getCases(forceRefresh).then(function (data) {
                vm.cases = data;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }

        vm.browse = function(url) {
    		$cordovaInAppBrowser.open(url, '_system', options)
		      .then(function(event) {
		        // success
		      })
		      .catch(function(event) {
		        // error
		      });
		};
        vm.loadCases(false);
    }
})();