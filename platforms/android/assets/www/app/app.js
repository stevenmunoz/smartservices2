angular.module('iot', ['ionic', "angular-data.DSCacheFactory", "uiGmapgoogle-maps", "ngCordova", "angularCharts"])

.run(function($ionicPlatform, DSCacheFactory) {
  
  DSCacheFactory("faqsDataCache", { storageMode: "localStorage", maxAge: 600000, deleteOnExpire: "aggressive" });
  DSCacheFactory("loginDataCache", { storageMode: "localStorage"});
  DSCacheFactory("pprsDataCache", { storageMode: "localStorage", maxAge: 600000, deleteOnExpire: "aggressive" });
  DSCacheFactory("servicePointsDataCache", { storageMode: "localStorage", maxAge: 20000, deleteOnExpire: "aggressive" });
  DSCacheFactory("statesDataCache", { storageMode: "localStorage" });
  DSCacheFactory("casesDataCache", { storageMode: "localStorage", maxAge: 600000, deleteOnExpire: "aggressive" });
  DSCacheFactory("newsDataCache", { storageMode: "localStorage", maxAge: 600000, deleteOnExpire: "aggressive" });
  DSCacheFactory("userDataCache", { storageMode: "localStorage"});
  DSCacheFactory("faqsDetailDataCache", { storageMode: "localStorage"});
  DSCacheFactory("diagnosticDataCache", { storageMode: "localStorage", maxAge: 600000, deleteOnExpire: "aggressive" });
  
  $ionicPlatform.ready(function() {
    
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  
  $stateProvider
    .state('router', {
      url: "/route",
      abstract: true,
      templateUrl: "templates/side-menu-left.html"
    })
    .state('router.dashboard', {
      url: "/dashboard",
	    abstract: true,
      views: {
        'menuContent' :{
          templateUrl: "templates/main-tab.html"
        }
      }
    })
    .state('router.dashboard.home', {
      url: "/home",
      views: {
        'home-tab' :{
          templateUrl: "app/home/home.html"
        }
      }
    })
    .state('router.about', {
      url: "/about",
      views: {
        'menuContent' :{
          templateUrl: "app/about/about.html"
        }
      }
    })
    .state('router.messages', {
      url: "/messages",
      abstract: true,
      views: {
        'menuContent' :{
          templateUrl: "app/messages/messages-tab.html"
        }
      }
    })
    .state('router.messages.inbox', {
      url: "/inbox",
      views: {
          'inbox-tab' :{
            templateUrl: "app/messages/inbox.html"
        }
      }
    })
    .state('router.messages.outbox', {
      url: "/outbox",
      views: {
          'outbox-tab' :{
            templateUrl: "app/messages/outbox.html"
        }
      }
    })
    .state('router.message-detail', {
      url: "/message/detail/:id/:emisor",
      views: {
          'menuContent' :{
            templateUrl: "app/messages/detail.html"
        }
      }
    })
    .state('router.messages.send', {
      url: "/send",
      views: {
          'send-tab' :{
            templateUrl: "app/messages/send.html"
        }
      }
    })
  	.state('router.faqs', {
        url: "/faqs",
        views: {
          'menuContent': {
            templateUrl: "app/faqs/faqs.html"  
          }
        }
    })
    .state('router.faqs-detail', {
        url: "/detail",
        views: {
          'menuContent': {
            templateUrl: "app/faqs/detail.html"  
          }
        }
    })
    .state('router.news', {
        url: "/news",
        views: {
          'menuContent': {
            templateUrl: "app/news/news.html"  
          }
        }
    })
   	.state('router.locations', {
        url: "/locations",
        views: {
          'menuContent' :{
            templateUrl: "app/servicepoints/servicepoints.html"
          }
        }
    })
  	.state('router.cases', {
        url: "/cases",
        views: {
          'menuContent': {
            templateUrl: "app/cases/cases.html"  
          }
        }
    })
    .state('router.diagnostic', {
        url: "/diagnostic",
        views: {
          'menuContent': {
            templateUrl: "app/diagnostic/diagnostic.html"  
          }
        }
    })
    .state('router.dtldiagnostic', {
        url: "/diagnostic",
        views: {
          'menuContent': {
            templateUrl: "app/diagnostic/dtldiagnostic.html"  
          }
        }
    })
  
  $urlRouterProvider.otherwise("/route/dashboard/home");
})

.constant('config', {

  //Endpoints
  backendEndpoint: '',
  testEndpoint: 'http://private-f1a15-mintransporte.apiary-mock.com/',
  countriesEndpoint: 'http://apps3diseno.co/ucn/world_c_s_c/',
  
  //faqs
  getFaqsUrl: "getallpreguntasfrecuentesactivas",
  
  //location
  getPoints: "points",
  getNearestPoint: "",
  getReportType: "reporttype",
  
  //countries
  getStates: "?_=states&c=CO",
  getCities: "?_=cities&s=",
  
  //Token key-values
  tokenType: 'tokenType',
  tokenKey: 'accessToken',
  
  //Rss reader
  googleRssToJsonApiUrl: 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=',
  googleRssVersion: '1.0',
  
  //news
  getNews: "news",
  
  //Rss cases
  getCases: "cases",

  //diagnostic
  getDiagnostic: "diagnostic",

  //Messages
  getUnreadMessagesUrl: "getmensajessinleerbyusuario",
  getMessageUrl: "getmensaje",
  inboxUrl: "getallmensajesbyreceptor",
  outboxUrl: "getallmensajesbyemisor",
  sendMessageUrl: "enviarmensaje",

  //Maps config
  latitude: '4.283435',
  longitude: '-74.22404',
  zoom: 8,
  //In app browser config
  inAppOptions: {
    location: 'yes',
    clearcache: 'yes',
    toolbar: 'yes',
    closebuttoncaption: 'Volver'
  }

})

.directive('wrapOwlcarousel', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            var options = scope.$eval($(element).attr('data-options'));
            $(element).owlCarousel(options);
        }
    };
});