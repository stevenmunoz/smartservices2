(function() {
	'use strict';

	angular.module('iot').factory('userApi', ['$http', '$q', 'DSCacheFactory', 'config', userApi]);

	function userApi ($http, $q, DSCacheFactory, config) {

		function saveUser (coda, name, role) {

			var deferred = $q.defer(),
				cacheKey = "messages";
				
			var data = {
			  "coda": coda,
			  "nombre": name,
			  "tipoId": 0,
			  "tipo": role
			};
	
			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.saveUser,
				headers: {'Content-Type': 'application/json'},
				data: data
			};
			
			$http(req).success(function (data) {
				deferred.resolve(data);
			}).error(function (data) {
				deferred.reject();
			});
				
			return deferred.promise;
		}

		return {
			saveUser: saveUser 
		};
	};
})();