(function() {
	'use strict';

	angular.module('iot').factory('faqsApi', ['$http', '$q', '$ionicLoading', 'DSCacheFactory', 'config', faqsApi]);
		
	function faqsApi($http, $q, $ionicLoading, DSCacheFactory, config) {

		self.faqsDataCache = DSCacheFactory.get("faqsDataCache");
		
		self.faqsDataCache.setOptions({
            onExpire: function (key, value) {
                getFaqs(true)
                    .then(function () {
                        console.log("Faqs Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting data. Putting expired item back in the cache.", new Date());
                        self.faqsDataCache.put(key, value);
                    });
            }
        });

     	function getFaqs (forceRefresh) {
			
			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

			var deferred = $q.defer(),
				cacheKey = "faqs",
				faqsData = null;

			if (!forceRefresh) {

				faqsData = self.faqsDataCache.get(cacheKey);
			}

			//is there faqs inside cache ?
			if(faqsData) {
				console.log("Found faqs cache");
				deferred.resolve(faqsData);
			} else {
				
				$ionicLoading.show({
              		template: 'Cargando Información ...'
            	});
				$http.get(config.backendEndpoint + config.getFaqsUrl)
					.success(function (data) {
						$ionicLoading.hide();
						console.log("receive data from http");
						self.faqsDataCache.put(cacheKey, data.result);
						deferred.resolve(data.result);
					})
					.error(function () {
						console.log("Error making http call on service");
						$ionicLoading.hide();

						faqsData = self.faqsDataCache.get(cacheKey);

						if(faqsData) {
							console.log("Found faqs inside cache");
							deferred.resolve(faqsData);
						} else {
							deferred.reject();	
						}
					});
			}

			return deferred.promise;
		};

		//reveal module pattern 	
		return {
			getFaqs: getFaqs 
		};
	};

})();