(function () {
    'use strict';
    angular.module('iot')
    .controller('MainCtrl',  ['$scope', '$rootScope', '$ionicSideMenuDelegate', '$ionicPopover', '$state', '$timeout', 'faqsApi', 'introApi', 'DSCacheFactory', function($scope, $rootScope, $ionicSideMenuDelegate, $ionicPopover, $state, $timeout, faqsApi, introApi, DSCacheFactory) {
        $scope.device = { id: null, name: 'No Device', icon: 'ion-ios7-help-empty', status: 'Offline' },
        
        self.userDataCache = DSCacheFactory.get("userDataCache");

        $scope.userName = self.userDataCache.get("name");

        $scope.toggleLeft = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };
        $scope.menuTap = function (route) {
            $state.go(route);
        };
        $ionicPopover.fromTemplateUrl('templates/alerts.html', {
            scope: $scope,
        }).then(function(popover) {
            $scope.popover = popover;
        });
        $scope.openAlerts = function($event) {
            $scope.popover.show($event);
        };
        $scope.closeAlerts = function() {
            $scope.popover.hide();
        };

        $scope.showMessages = function() {
            $state.go('router.messages.inbox');
            $scope.closeAlerts();
        };
        $scope.$on('$destroy', function() {
            $scope.popover.remove();
        });

        $scope.logout = function() {

            //$state.go('intro');

            introApi.logout().then(function (data){
                vm.logoutData = data;
                $state.go('intro');
            }, function(reason) {
                console.log("An error has ocurred performing logout " + reason);
            });
        };

        $timeout(function () {
            ionic.EventController.trigger("resize", "", true, false);
        }, 1500);
    }])
})();