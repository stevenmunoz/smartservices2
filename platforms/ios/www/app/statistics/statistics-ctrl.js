(function () {
    'use strict';

    angular.module('iot').controller('Charts', ['$scope', '$interval', '$rootScope', '$state', 'newsApi', '$cordovaInAppBrowser', 'config', Charts]);

    function Charts ($scope, $interval, $rootScope, $state, newsApi, $cordovaInAppBrowser, config) {

        /*$scope.linelabels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		$scope.lineseries = ['Series A', 'Series B'];
		$scope.linedata = [
			[65, 76, 50, 47, 36, 30, 25, 48, 56, 55, 59, 63],
			[50, 48, 40, 57, 86, 99, 90, 58, 48, 80, 57, 60]
		];*/
		$scope.barlabels = ['2012', '2013', '2014'];
		$scope.barseries = ['Accidentes', 'Lesionados', 'Fallecidos'];
		$scope.bardata = [
			[23854, 28169, 32182],
			[18204, 22559, 22868],
			[2237, 2277, 1920]
		];
		/*$scope.polarLabels = ["Water", "Energy", "Gas", "Internet", "Fees"];
		$scope.polarData = [300, 500, 100, 40, 120];*/
		$scope.doughnutLabels = ["Conductor", "Pasajero", "Peatón", "Sin Dato"];
		$scope.doughnutData = [300, 500, 100, 20];
		$scope.options =  {
			responsive: true,
			showTooltips: true,
			animation: true,
			pointDot : true,
			scaleShowLabels: true,
			showScale: true,
			maintainAspectRatio: false,
			datasetStrokeWidth : 1,
	    };
		$interval(function () {
			$scope.doughnutData = [];
			$scope.polarData = [];
			
			for (var i = 0; i < 5; i++) {
				$scope.polarData.push(Math.floor(Math.random() * 500));
			}
			for (var i = 0; i < 4; i++) {
				$scope.doughnutData.push(Math.floor(Math.random() * 500));
			}
		}, 2500);

    }
})();