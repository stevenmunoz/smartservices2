(function () {
    'use strict';

    angular.module('iot').controller('FaqsDetailCtrl', ['$scope', '$state', 'DSCacheFactory', FaqsDetailCtrl]);

    function FaqsDetailCtrl($scope, $state, DSCacheFactory) {

        self.faqsDetailDataCache = DSCacheFactory.get("faqsDetailDataCache");

        var vm = this;

        vm.question = self.faqsDetailDataCache.get("question");
        vm.answer = self.faqsDetailDataCache.get("answer");
        
    };
})();