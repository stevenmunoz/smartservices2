(function () {
    'use strict';

    angular.module('iot').controller('DtlDiagnosticCtrl', ['$scope', '$rootScope', '$state', '$timeout', '$ionicPopup', DtlDiagnosticCtrl]);

    function DtlDiagnosticCtrl ($scope, $rootScope, $state, $timeout, $ionicPopup) {
    	var vm = this;
    	
    	//Titulo para indicar si esta listo para viajar
    	vm.title = "NO ESTA LISTO PARA VIAJAR";

    	var sizeList = $rootScope.sizeList;
    	var listAdviceTemp = $rootScope.listDiagnostic;

    	if(!containsObject("0", listAdviceTemp)) {
    		vm.percentage = Math.round((listAdviceTemp.length * 100)/sizeList);
    		vm.style = "width:" + vm.percentage + "%";
    		vm.listAdvice = listAdviceTemp;

    		if(vm.percentage >= 80) {
    			vm.title = "LISTO PARA VIAJAR";
    		}
    	}
    	else {
    		console.log("No puede manejar");
    		vm.percentage = 0;
    		vm.style = "width:0%";

    		vm.listAdvice = _.filter(listAdviceTemp, function(item){
            	return item.type === "0";
            });

    	}

    	vm.altImage = vm.listAdvice[Math.floor(Math.random()*vm.listAdvice.length)].image;

    	//Valia si existe un objeto en la lista con type = 0
    	function containsObject(attr, list) {
		    var i;
		    for (i = 0; i < list.length; i++) {
		        if (list[i].type === attr) {
		            return true;
		        }
		    }
		    return false;
		}
    }
})();