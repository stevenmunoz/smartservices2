(function () {
    'use strict';

    angular.module('iot').controller('DiagnosticCtrl', ['$scope', '$rootScope', '$state', '$timeout', '$ionicPopup', 'DSCacheFactory', 'diagnosticApi', DiagnosticCtrl]);

    function DiagnosticCtrl ($scope, $rootScope, $state, $timeout, $ionicPopup, DSCacheFactory, diagnosticApi) {

    	var vm = this;

        vm.url = "router.dtldiagnostic";
        vm.dsblBtnDiagnostic = true;
        var tempListDiagnostic = [];

        vm.getDiagnostic = function (forceRefresh) {

            diagnosticApi.getDiagnostic(forceRefresh).then(function (data) {
                vm.listDiagnostic = data;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }

        vm.addList = function (object) {
            if($("#"+ object.id +"").is(':checked')) {
                tempListDiagnostic.push(object);
            }
            else {
                vm.deleteObject(object, tempListDiagnostic);
            }
            
            //Valido si la lista contiene mas de 4 elementos para habilitar el botón
            vm.validateList();
        };

        vm.deleteObject = function(obj, list) {
            var i;
            for (i = 0; i < list.length; i++) {
                if (list[i] === obj) {
                    list.splice(i, 1);
                }
            }
        };

        vm.validateList = function() {
            if(tempListDiagnostic.length >= 4) {
                vm.dsblBtnDiagnostic = false;
            }
            else {
                vm.dsblBtnDiagnostic = true;
            }
        };

        vm.menuTap = function (route) {
            $rootScope.sizeList = vm.listDiagnostic.length;
            $rootScope.listDiagnostic = tempListDiagnostic;
            $state.go(route);
        };

        vm.getDiagnostic(false);
    }
})();