(function() {
	'use strict';

	angular.module('iot').factory('driverApi', ['$http', '$q', '$ionicLoading', 'DSCacheFactory', 'config', '$timeout', driverApi]);
		
	function driverApi($http, $q, $ionicLoading, DSCacheFactory, config, $timeout) {

		function sendRate(dataRate)
		{
			var deferred = $q.defer();

			$ionicLoading.show({
              	template: 'Registrando Calificación ...'
            });

            var data = {
            	"plate": dataRate.plate,
            	"type": dataRate.type.name,
 				"company": dataRate.company,
 				"observation": dataRate.observation
			};

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.saveDriver,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req).success (function (data) {
				$ionicLoading.hide();
					
				if (!data.success) {

					$ionicLoading.show({
                		template: 'Ha ocurrido un error registrando la calificación, intente en unos minutos.'
	                });
	                $timeout( function() {
	                    $ionicLoading.hide();
	                    deferred.reject();	
	                }, 2000);
				} else {

					deferred.resolve(data);
				}
			})
			.error(function () {
				console.log("Error register rate");
				deferred.reject();		
			});
			
			return deferred.promise;
		}


		//reveal module pattern 	
		return {
			sendRate: sendRate
		};
	};

})();