(function() {
	'use strict';

	angular.module('iot').factory('pointsApi', ['$http', '$q', '$ionicLoading', 'DSCacheFactory', 'config', '$timeout', pointsApi]);
		
	function pointsApi($http, $q, $ionicLoading, DSCacheFactory, config, $timeout) {

		self.servicePointsDataCache = DSCacheFactory.get("servicePointsDataCache");
		self.loginDataCache = DSCacheFactory.get("loginDataCache");
		
		/*
			Description:
			Cache on expire configuration, reload data again if fails, put back in cache latest key value
		*/
		self.servicePointsDataCache.setOptions({
            onExpire: function (key, value) {
                getPoints()
                    .then(function () {
                        console.log("Points Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting points data. Putting expired item back in the cache.", new Date());
                        self.servicePointsDataCache.put(key, value);
                    });
            }
        });

		/*
			Description:
			Retorn all points from service
		*/
     	function getPoints () {
			
			var deferred = $q.defer(),
				cacheKey = "points",
				pointsData = self.servicePointsDataCache.get(cacheKey),
				loginData = self.loginDataCache;

			//is there points inside cache ?
			if(pointsData) {
				console.log("Found points inside cache");
				deferred.resolve(pointsData);
			} else {
				
				/*$ionicLoading.show({
              		template: 'Verificando Riesgos Cercanos ...'
            	});*/

				var req = {
					method: 'GET',
					url: config.backendEndpoint + config.getPoints,
					params: {}
				};

				$http(req).success (function (data) {
						$ionicLoading.hide();
						console.log("receive points from http");
						self.servicePointsDataCache.put(cacheKey, data.result.points);
						deferred.resolve(data.result.points);
					})
					.error(function () {
						console.log("Error making http call on points service");
						$ionicLoading.hide();

						pointsData = self.servicePointsDataCache.get(cacheKey);

						if(pointsData) {
							console.log("Found points data inside cache");
							deferred.resolve(pointsData);
						} else {
							deferred.reject();	
						}
					});
			}

			return deferred.promise;
		};

		/*
			Description:
			Retorn nearest point
		
			Params:
			@latitude: actual point latitude
			@longitude: actual point longitude
		*/
		function getNearestPoint (latitude, longitude) {
			
			var deferred = $q.defer(),
				loginData = self.loginDataCache;

			//is there points inside cache ?
				
			$ionicLoading.show({
              	template: 'Cargando ACR Cercano ...'
            });

			var req = {
				method: 'GET',
				url: config.acrEndPoint + config.getNearestPoint,
				headers: {'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)},
				params: {longitud:longitude, latitud:latitude}
			};

			$http(req).success (function (data) {
				$ionicLoading.hide();
				console.log("receive nearest point");
				deferred.resolve(data);
			})
			.error(function () {
				console.log("Error receiving nearest point");
				$ionicLoading.hide();
				deferred.reject();	
			});
			

			return deferred.promise;
		};

		function getReportType () {
			
			var deferred = $q.defer(),
				cacheKey = "reportType",
				reportTypeData = self.servicePointsDataCache.get(cacheKey),
				loginData = self.loginDataCache;

			//is there points inside cache ?
			if(reportTypeData) {
				deferred.resolve(reportTypeData);
			} else {
				
				var req = {
					method: 'GET',
					url: config.testEndpoint + config.getReportType,
					params: {}
				};

				$http(req).success (function (data) {
						$ionicLoading.hide();
						self.servicePointsDataCache.put(cacheKey, data);
						deferred.resolve(data);
					})
					.error(function () {
						$ionicLoading.hide();

						reportTypeData = self.servicePointsDataCache.get(cacheKey);

						if(reportTypeData) {
							deferred.resolve(reportTypeData);
						} else {
							deferred.reject();	
						}
					});
			}

			return deferred.promise;
		};

		function sendPoint(dataPoint)
		{
			var deferred = $q.defer();

			$ionicLoading.show({
              	template: 'Registrando Riesgo Cercano ...'
            });

            var data = {
            	"latitude": dataPoint.coordinates.latitude,
            	"longitude": dataPoint.coordinates.longitude,
 				"description": dataPoint.comment,
 				"name": dataPoint.reportType.text
			};

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.savePoints,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req).success (function (data) {

				$ionicLoading.hide();
				
				if (!data.success) {

					$ionicLoading.show({
                		template: 'Ha ocurrido un error registrando el riesgo cercano, intente en unos minutos.'
                	});
	                $timeout( function() {
	                    $ionicLoading.hide();
	                    deferred.reject();	
	                }, 2000);

				} else {

					self.servicePointsDataCache.put("points", "");
					deferred.resolve(data);
				}
			})
			.error(function () {
				console.log("Error register point");
				deferred.reject();		
			});
			
			return deferred.promise;
		}


		//reveal module pattern 	
		return {
			getPoints: getPoints,
			getNearestPoint: getNearestPoint,
			getReportType: getReportType,
			sendPoint: sendPoint
		};
	};

})();