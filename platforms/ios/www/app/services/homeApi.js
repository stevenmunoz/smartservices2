(function(){
	'use strict';

	angular.module('iot').factory('homeApi', ['$http', '$q', 'DSCacheFactory', '$ionicLoading', 'config', homeApi]);

	function homeApi ($http, $q, DSCacheFactory, $ionicLoading, config) {

		self.homeDataCache = DSCacheFactory.get("homeDataCache");
		
		self.homeDataCache.setOptions({
            onExpire: function (key, value) {
                getSlider(true)
                    .then(function () {
                        console.log("Slider data Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting slider data. Putting expired item back in the cache.", new Date());
                        self.homeDataCache.put(key, value);
                    });
            }
        });

        function getSlider (forceRefresh) {

        	$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

        	var deferred = $q.defer(),
        		requestUrl = config.testEndpoint,
				cacheKey = "news",
				sliderData = null;

			var req = {
				method: 'GET',
				url: requestUrl + config.getSlider
			};
			
			//Request for slider

			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

		     	if (!forceRefresh) {
					sliderData = self.homeDataCache.get(cacheKey);
				}

				if (sliderData) {
					$ionicLoading.hide();
					console.log("Found slider data inside cache");
					deferred.resolve(sliderData);

				} else {
						
					$ionicLoading.show({
		              	template: 'Cargando Información ...'
		            });

					$http(req).success(function (data) {
						$ionicLoading.hide();
						self.homeDataCache.put(cacheKey, data);
						deferred.resolve(data);

					}).error(function (data) {
							
						$ionicLoading.hide();
						sliderData = self.homeDataCache.get(cacheKey);

						if(sliderData) {
							deferred.resolve(sliderData);
						} else {
							deferred.reject();	
						}
					});
				}

				return deferred.promise;
	    }

        return {
        	getSlider: getSlider,
        };
	}

})();