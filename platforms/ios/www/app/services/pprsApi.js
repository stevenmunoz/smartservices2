(function () {
	'use strict';

	angular.module('iot').factory('pprsApi', ['$http', '$q', 'DSCacheFactory', '$ionicLoading', 'config', pprsApi]);

	function pprsApi ($http, $q, DSCacheFactory, $ionicLoading, config) {

		self.pprsDataCache = DSCacheFactory.get("pprsDataCache");
		self.loginDataCache = DSCacheFactory.get("loginDataCache");
		self.professionalDataCache = DSCacheFactory.get("professionalDataCache");
		
		function getPprs (forceRefresh, idProfessional) {

        	if (typeof forceRefresh === "undefined") { forceRefresh = false; }

        	var deferred = $q.defer(),
				cacheKey = "pprs",
				pprsData = null,
				loginData = self.loginDataCache;;

			if (!forceRefresh) {

				pprsData = self.pprsDataCache.get(cacheKey);
			}

			if(pprsData) {
				
				console.log("Found pprs data inside cache");
				deferred.resolve(pprsData);

			} else {
				
				$ionicLoading.show({
              		template: 'Cargando Información ...'
            	});

				var req = {
					method: 'GET',
					url: config.acrEndPoint + config.getPprsUrl,
					headers: {'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)},
					params: {IdProfesional: idProfessional}
				};
				
				$http(req).success(function (data) {
					$ionicLoading.hide();
					self.pprsDataCache.put(cacheKey, data);
					deferred.resolve(data);

				}).error(function (data) {
					
					$ionicLoading.hide();
					pprsData = self.pprsDataCache.get(cacheKey);

					if(pprsData) {
						deferred.resolve(pprsData);
					} else {
						deferred.reject();	
					}
				});
			}

			return deferred.promise;
        };

        function getProfessionalByPpr (idPpr) {

        	var deferred = $q.defer(),
				cacheKey = "professional",
				profData = self.professionalDataCache.get(cacheKey),
				loginData = self.loginDataCache;;

			if(profData) {
				
				console.log("Found professional data inside cache");
				deferred.resolve(profData);

			} else {
				
				$ionicLoading.show({
              		template: 'Cargando Información ...'
            	});

				var req = {
					method: 'GET',
					url: config.acrEndPoint + config.getProfessionalByPpr,
					headers: {'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)},
					params: {IdPPR: idPpr}
				};
				
				$http(req).success(function (data) {
					$ionicLoading.hide();
					self.professionalDataCache.put(cacheKey, data);
					deferred.resolve(data);

				}).error(function (data) {
					
					$ionicLoading.hide();
					profData = self.professionalDataCache.get(cacheKey);

					if(profData) {
						deferred.resolve(profData);
					} else {
						deferred.reject();	
					}
				});

			}

			return deferred.promise;
        };

        function getLastScore(user) {

        	var deferred = $q.defer(),
				loginData = self.loginDataCache;
			
			$ionicLoading.show({
          		template: 'Cargando Información ...'
        	});

        	var data = {
			  "usuario": user
			};

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.getScorePprUrl,
				headers: {'Content-Type': 'application/json'},
				data: data
			};
			
			$http(req).success(function (data) {
				$ionicLoading.hide();
				deferred.resolve(data);

			}).error(function (data) {
				$ionicLoading.hide();
				deferred.reject();
			});

			return deferred.promise;
        };

        return {
        	getPprs: getPprs,
        	getProfessionalByPpr: getProfessionalByPpr,
        	getLastScore: getLastScore
        };
	}

})();