(function(){
	'use strict';

	angular.module('iot').factory('casesApi', ['$http', '$q', 'DSCacheFactory', '$ionicLoading', 'config', '$timeout', casesApi]);

	function casesApi ($http, $q, DSCacheFactory, $ionicLoading, config, $timeout) {

		self.casesDataCache = DSCacheFactory.get("casesDataCache");
		
		self.casesDataCache.setOptions({
            onExpire: function (key, value) {
                getCases(true)
                    .then(function () {
                        console.log("Cases data Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting cases data. Putting expired item back in the cache.", new Date());
                        self.casesDataCache.put(key, value);
                    });
            }
        });

        function getCases (forceRefresh) {

        	$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

        	var deferred = $q.defer(),
        		requestUrl = config.backendEndpoint,
        		//requestUrl = config.testEndpoint,
				cacheKey = "cases",
				casesData = null;

			var req = {
				method: 'GET',
				url: requestUrl + config.getCases
			};
			
				
			//Request for cases

			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

	        if (!forceRefresh) {

				casesData = self.casesDataCache.get(cacheKey);
			}

			if(casesData) {
				$ionicLoading.hide();
				console.log("Found cases data inside cache");
				deferred.resolve(casesData);

			} else {
					
				$http(req).success(function (data) {
					$ionicLoading.hide();
					self.casesDataCache.put(cacheKey, data.result.cases);
					deferred.resolve(data.result.cases);

				}).error(function (data) {
						
					$ionicLoading.hide();
					casesData = self.casesDataCache.get(cacheKey);

					if(casesData) {
						deferred.resolve(casesData);
					} else {
						deferred.reject();	
					}
				});
			}

			return deferred.promise;
		}

		function getCategoryDiagnostic () {
			
			var deferred = $q.defer(),
				cacheKey = "categoryDiagnostic",
				categoryDiagnosticData = self.casesDataCache.get(cacheKey),
				loginData = self.loginDataCache;

			//is there points inside cache ?
			if(categoryDiagnosticData) {
				deferred.resolve(categoryDiagnosticData);
			} else {
				
				var req = {
					method: 'GET',
					url: config.testEndpoint + config.getCategoryDiagnostic,
					params: {}
				};

				$http(req).success (function (data) {
						$ionicLoading.hide();
						self.casesDataCache.put(cacheKey, data);
						deferred.resolve(data);
					})
					.error(function () {
						$ionicLoading.hide();

						categoryDiagnosticData = self.casesDataCache.get(cacheKey);

						if(categoryDiagnosticData) {
							deferred.resolve(categoryDiagnosticData);
						} else {
							deferred.reject();	
						}
					});
			}

			return deferred.promise;
		}

		function sendCase(dataCase)
		{
			var deferred = $q.defer();

			$ionicLoading.show({
              	template: 'Registrando Historia ...'
            });

            var data = {
 				"title": dataCase.title, 
 				"description": dataCase.description,
 				"image": dataCase.image
			};

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.saveCases,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req).success (function (data) {
				$ionicLoading.hide();

				if (!data.success) {

					$ionicLoading.show({
                		template: 'Ha ocurrido un error registrando la historia, intente en unos minutos.'
	                });
	                $timeout( function() {
	                    $ionicLoading.hide();
	                    deferred.reject();	
	                }, 2000);
				} else {

					deferred.resolve(data);
				}
			})
			.error(function () {
				console.log("Error register history");
				deferred.reject();		
			});
			
			return deferred.promise;
		}

        return {
        	getCases: getCases,
        	getCategoryDiagnostic: getCategoryDiagnostic,
        	sendCase: sendCase
        };
	}

})();