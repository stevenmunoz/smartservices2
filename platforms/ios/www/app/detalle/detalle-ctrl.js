
(function () {
    'use strict';

    angular.module('iot').controller('SlideController', ['$scope', SlideController]);

    function SlideController ($scope) {

      $scope.myActiveSlide = 1;
    }
})();