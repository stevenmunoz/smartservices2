angular.module('ionicApp', ['ionic'])

.controller('MyCtrl', function ($scope) {
  // don't be scared by the image value, its just datauri

  $scope.items = [
    {
      id: 1,
      desc: 'Descripcion 1',
      image:'../img/Foto1'
      },
	{
      id:2,
      desc: 'Descripcion 2',
      image:'../img/Foto2'
      },
{
      id: 3,
      desc: 'Descripcion 3',
      image:'../img/Foto3'
      },
{
      id: 4,
      desc: 'Descripcion 4',
      image:'../img/Foto4'
      },
{
      id: 5,
      desc: 'Descripcion 5',
      image:'../img/Foto5'
      },
{
      id: 6,
      desc: 'Descripcion 6',
      image:'../img/Foto6'
      },
{
      id: 7,
      desc: 'Descripcion 7',
      image:'../img/Foto7'
      },
{
      id: 8,
      desc: 'Descripcion 8',
      image:'../img/Foto8'
      },
{
      id: 9,
      desc: 'Descripcion 9',
      image:'../img/Foto9'
      },
{
      id: 10,
      desc: 'Descripcion 10',
      image:'../img/Foto10'
      }
      ];
    }
);